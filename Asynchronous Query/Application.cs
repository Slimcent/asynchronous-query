﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynchronous_Query.Database;

namespace Asynchronous_Query
{
    public static class Application
    {
        public static async void japa()
        {
            Validation.PromptUser();
            var username = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(username) ||username.Length > 10 || username.Length < 3)
            {
                Validation.InvalidPrompt("username");
                username = Console.ReadLine();
                Console.ResetColor();
            }
            Console.ResetColor();
            await Login.LogUser(username);

            Console.WriteLine("Enter your Password");
            var password = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(password) || password.Length > 10 || password.Length < 3)
            {
                Validation.InvalidPrompt("password");
                password = Console.ReadLine();
                Console.ResetColor();
            }
            Console.ResetColor(); 
            await Login.GetUser(username, password);

            LogOut.BackToStart();
           
        }  
    }
}
