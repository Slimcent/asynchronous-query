﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynchronous_Query.Database;

namespace Asynchronous_Query
{
    public static class Login
    {
        private static AccountsDb account = new AccountsDb();
        private static UsersDb usersDb = new UsersDb();
        public static Task<bool> LogUser(string username)
        {
            var query1 = from user in account.Bezao()
                         where user.Username.Equals(username)
                         select user.AccountName;
            while (query1.Count() == 0)
            {
                Console.WriteLine("No match found! Re-enter your Username");
                username = Console.ReadLine();               
                Console.ResetColor();
                return Task.FromResult(false);
            }
            return Task.FromResult(true);
            
        }

        public static Task GetUser(string uname, string pword)
        {
            var query = from user in usersDb.Genesys()
                        where user.UserName.Equals(uname) && user.Password.Equals(pword)
                        select new { user.UserName, user.Email, user.AccountNumber, user.Address };
            while (query.Count() == 0)
            {
                Console.WriteLine("No match found! Re-enter your password");
                pword = Console.ReadLine();
            }
            foreach (var dev in query)
            {
                Console.WriteLine("\nUser Complete details below");
                Console.WriteLine($"Your Username is {dev.UserName}");
                Console.WriteLine($"Your Account number is {dev.AccountNumber}");
                Console.WriteLine($"Your Email is {dev.Email}");
                Console.WriteLine($"Your Address is {dev.Address}\n");
                //Console.ReadLine();
            }
            return Task.CompletedTask;
        }
    }
}
