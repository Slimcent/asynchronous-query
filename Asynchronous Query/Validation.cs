﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asynchronous_Query
{
    public static class Validation
    {
        public static void PromptUser()
        {
            var menu = new StringBuilder();
            menu.Append("Hello!!! Welcome to Razor Bank\n");
            menu.AppendLine("Enter your Username");
            Console.WriteLine(menu.ToString());
        }
        public static void InvalidPrompt(string field)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{field} is invald, Re-enter your {field}");
        }
    }
}
