﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynchronous_Query.models;

namespace Asynchronous_Query.Database
{
    public class UsersDb
    {
        public IEnumerable<UsersData> Genesys()
        {

            return new List<UsersData>()
            {
                new UsersData{ID = 1, UserName = "chikky", AccountNumber = 30379928020, Email = "Chikky@gmal.com", Address = "Enugu", Password = "chikki"},
                new UsersData{ID = 2, UserName = "sammy", AccountNumber = 30370928088, Email = "sam@gmail.com", Address = "Lagos",  Password = "sammy"},
                new UsersData{ID = 3, UserName = "jane", AccountNumber = 30370928066, Email = "jane@yahoo.com", Address = "Abuja", Password = "jane"},
                new UsersData{ID = 4, UserName = "chinedu", AccountNumber = 30388928020, Email = "edu@hotmail.com", Address = "Enugu", Password = "edu"},
                new UsersData{ID = 5, UserName = "bill", AccountNumber = 30370028020, Email = "bills@gmail.com", Address = "Ph",  Password = "finger"},
                new UsersData{ID = 6, UserName = "james", AccountNumber = 30990928020, Email = "james@gmal.com", Address = "Imo", Password = "shilo"},
                new UsersData{ID = 7, UserName = "dara", AccountNumber = 30370928000, Email = "dara@hotmail.com", Address = "Lagos", Password = "dara"},
            };
        }
    }
}
