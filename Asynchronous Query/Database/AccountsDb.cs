﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynchronous_Query.models;

namespace Asynchronous_Query.Database
{
    public class AccountsDb
    {
        public IEnumerable<Accounts> Bezao()
        {

            return new List<Accounts>()
            {
                new Accounts{ID = 1, AccountName = "Chinese Chikky", Username = "chikky"},
                new Accounts{ID = 2, AccountName = "Scientific Sammy", Username = "sammy"},
                new Accounts{ID = 3, AccountName = "Janet Jane", Username = "jane"},
                new Accounts{ID = 4, AccountName = "Calculative Chinedu", Username = "edu"},
                new Accounts{ID = 5, AccountName = "Underrated Uriel", Username = "finger"},
                new Accounts{ID = 6, AccountName = "sincere Shola",Username = "shilo"},
                new Accounts{ID = 7, AccountName = "Daring Dara", Username = "dara"},
            };
        }
    }
}
