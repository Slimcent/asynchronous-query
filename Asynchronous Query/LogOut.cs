﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asynchronous_Query
{
    public static class LogOut
    {
        public static void BackToStart()
        {
            var menu = new StringBuilder();
            menu.Append("Press 1 to Log out\n");
            menu.AppendLine("Press 2 to Exit");
            Console.WriteLine(menu.ToString());
            var selection = Console.ReadLine();

            while (selection != "1" && selection != "2")
            {
                Console.WriteLine("Invalid selection made, Chose the right option");
                selection = Console.ReadLine();
            }

            switch (selection)
            {
                case "1":
                    Logout();
                    break;
                case "2":
                    Exit("2");
                    break;
                default:
                    Console.WriteLine("Invalid selection made, Chose the right option");
                    break;
            }
        }

        public static void Logout()
        {
            Console.Clear();
            Application.japa();
        }

        public static bool Exit(string num)
        {
            return string.Equals(num, "2", StringComparison.OrdinalIgnoreCase);
        }
    }
}
