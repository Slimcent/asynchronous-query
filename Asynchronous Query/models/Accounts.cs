﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asynchronous_Query.models
{
    public class Accounts
    {
        public int ID { get; set; }
        public string AccountName { get; set; }
        public string Username { get; set; }
    }
}
