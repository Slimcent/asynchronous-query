﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asynchronous_Query.models
{
    public class UsersData
    {
        public int ID { get; set; }
        public long AccountNumber { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
    }
}
